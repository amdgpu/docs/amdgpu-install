########
Overview
########

.. contents:: Table of Contents

----------

This guide provides installation and uninstallation procedures for the AMDGPU
stack.

For information about how to install ROCm on AMD desktop GPUs based on the
RDNA™ 3 architecture, see
`Use ROCm on Radeon GPUs <https://rocm.docs.amd.com/projects/radeon/en/latest/index.html>`_.
For more information about supported AMD Radeon™ desktop GPUs, see
`Radeon Compatibility Matrices <https://rocm.docs.amd.com/projects/radeon/en/latest/docs/compatibility.html>`_.

.. note:: The rest of this document will refer to Radeon™ Software for Linux®
     as the AMDGPU stack.

.. note:: Some components, such as OpenCL, are provided by the ROCm stack. For
     simplicity, this subset of ROCm will be included in the use of the term
     "AMDGPU stack". Please see the
     `ROCm documentation <https://rocmdocs.amd.com/en/latest/>`_ for further
     information on other ROCm components.

---------

Stack Use Cases
***************

There are two major use cases available for installation:

- **Workstation**: recommended for use with **Radeon Pro** graphics products.
- **All-Open**: recommended for use with consumer products.

+--------------------+------------------------------------------------+
| Install Use Case   | Components                                     |
+====================+================================================+
| All-Open           | * Base kernel drivers                          |
|                    | * Base accelerated graphics drivers            |
|                    | * Mesa OpenGL                                  |
|                    | * Mesa multimedia                              |
|                    | * OpenCL (optional)                            |
|                    |    - ROCr OpenCL stack (supports Vega 10 and   |
|                    |      later products)                           |
+--------------------+------------------------------------------------+
| Workstation        | * Base kernel drivers                          |
| (**Proprietary**)  | * Base accelerated graphics drivers            |
|                    | * Mesa multimedia                              |
|                    | * Workstation OpenGL (**Proprietary**)         |
|                    | * OpenCL (optional)                            |
|                    |    - ROCr OpenCL stack, supports Vega 10 and   |
|                    |      later products                            |
|                    |                                                |
|                    | * Pro Vulkan (optional, **Proprietary**)       |
+--------------------+------------------------------------------------+

You can install a combination of stack components using the ``amdgpu-install``
script by appending comma separated selections to ``--vulkan``, ``--opencl``,
or ``--usecase``, such as:

.. code-block:: bash

  $ amdgpu-install --usecase=graphics,opencl

See ``amdgpu-install -h`` for more options or ``amdgpu-install --list-usecase``
for more available usecases.

.. note:: Starting with the 21.40 release, ``amdgpu-install`` has changed and
    some options available in previous releases have been removed.
    ``amdgpu-pro-install`` has been dropped, but the following equivalent
    command can be used instead:
    ``amdgpu-install --usecase=workstation --vulkan=pro``.

.. note::
   - When installing the All-Open use case using ``amdgpu-install`` script,
     every component from the All-Open stack will be installed. There is no
     supported way to install arbitrary combinations of these components when
     using ``amdgpu-install`` but you can use the system package manager to do
     so.

   - When installing any **Proprietary** components for the first time, the
     user will be prompted with the End User License Agreement ("EULA") and
     asked to accept. To allow for non-interactive install, the option
     ``--accept-eula`` can be appended to ``amdgpu-install``, but using this
     option will prevent the proprietary packages or updates from being
     accessible directly with the system package manager. By using the
     ``--accept-eula`` option, you are confirming that you have read and
     agreed to be bound by the terms and conditions of the EULA
     (/usr/share/amdgpu-insall/AMDGPUPROEULA) for use of AMD Proprietary
     components

   - 32-bit graphics runtime libraries are automatically installed when
     installation is performed using ``amdgpu-install`` script. The addition of
     the "--no-32" option can be used to exclude these runtime libraries.
