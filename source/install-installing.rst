###########################################
Installing or Uninstalling the AMDGPU stack
###########################################

.. contents:: Table of Contents

.. note:: If you are installing on a low memory system, it is advised to
      increase swap size during installation to avoid out of memory issues.

----------

Installing the All-Open Use Case
================================

.. note:: This was referred to as the "All-Open Variant" in previous
      documentation

Run the following command to install the All-Open components:

.. code-block:: bash

  $ amdgpu-install -y --usecase=graphics


.. note:: The ``-y`` option installs non-interactively. See
      :doc:`Using the amdgpu-install script <install-script>` for more
      information.

--------

Reboot the target system after running ``amdgpu-install``.

Installing the Workstation Use Case
===================================

.. note:: This was referred to as the "Pro Variant" in previous documentation

Refer to the following table to understand how to install a combination of
Workstation (WS) components:

+-------------------------------------------------------------------------+-----------------+
|**Command**                                                              |**Installed      |
|                                                                         |Components**     |
+-------------------------------------------------------------------------+-----------------+
|``$ amdgpu-install -y  --usecase=workstation``                           |- Base kernel    |
|                                                                         |- Accelerated    |
|                                                                         |  graphics       |
|                                                                         |- Mesa           |
|                                                                         |  multimedia     |
|                                                                         |- WS OpenGL      |
+-------------------------------------------------------------------------+-----------------+
|``$ amdgpu-install --usecase=workstation -y --vulkan=pro --opencl=rocr`` |- Base kernel    |
|                                                                         |- Accelerated    |
|                                                                         |  graphics       |
|                                                                         |- Mesa           |
|                                                                         |  multimedia     |
|                                                                         |- WS OpenGL      |
|                                                                         |- Pro Vulkan     |
|                                                                         |- OpenCL         |
+-------------------------------------------------------------------------+-----------------+

Reboot the target system after running ``amdgpu-install``.

For more information, refer to the following sections to understand how to
install individual components.

Components
===========

OpenGL (Default Component)
--------------------------

OpenGL is a default component of both use cases.

.. _Installation-OpenCL-Optional-Component:

OpenCL (Optional Component)
---------------------------
OpenCL is an optional component and is installed only if it is specifically
requested.

Two different implementations of OpenCL (legacy and ROCr) are provided.
Either of these implementations or both of them can be installed
on the target system. Legacy is deprecated and will be removed in the future.
Once removed, legacy products will no longer be officially supported, but some
products might work on the ROCr OpenCL implementation with varying results.

To use the ROCr implementation of OpenCL, the running user might need
additional permissions depending on OS policy. If ``clinfo`` or any openCL
application does not work, check ownership and permissions of the render
nodes:

.. code-block:: bash

        ls -l /dev/dri/render*

If the render nodes are owned by group render but not readable and writable by
all users, consider adding the current user to the render group:

.. code-block:: bash

        sudo usermod -a -G render $LOGNAME

If you are an administrator, ``$LOGNAME`` can be replaced by any valid
username.

Alternatively, if the render nodes are owned by the video group but not
readable and writable by all users, consider adding the current user to the
video group:

.. code-block:: bash

        sudo usermod -a -G video $LOGNAME

Different Linux distributions have different ownership and permission policies
for render nodes. In addition, your organization may have its own policies
that override the distribution defaults. If unsure, consult the documentation
for your distribution or your organization.

In some scenarios, it may be desirable to install **only** the OpenCL portion
of the AMDGPU stack (omitting the OpenGL portion), which can be accomplished
by using the ``--usecase=opencl`` option instead. The typical use case is
headless compute.

Vulkan (Optional Component)
---------------------------

Vulkan is an optional component of the AMDGPU stack and is installed only if it
is specifically requested. The ``--vulkan`` option can be used to specify which
implementation of vulkan is desired.

See :ref:`Installation-Vulkan-Optional-Component` for installation options.


Secure Boot Support
===================

The kernel modules used by the AMDGPU stack rely on DKMS. DKMS will compile the
modules during installation and re-compile the modules when kernel is updated.
To use secure boot with DKMS, a machine owned key (MOK), which is unique to the
system, will be generated to sign the kernel modules.

Please see the section below that applies to the distro of the target system.

.. note:: If you already have a MOK enrolled on your system, the driver package
	expects the private key for this MOK to be installed in a specific
	location (see table below). Prior to installing the driver, either copy
	the private key to this location or create a symbolic link.

+----------------+-----------------------------------+
|**DISTRO**      |**MOK PRIVATE KEY**                |
+----------------+-----------------------------------+
|Ubuntu/Debian   | /var/lib/shim-signed/mok/MOK.priv |
+----------------+-----------------------------------+
|RHEL/CentOS     | /root/mok.priv                    |
+----------------+-----------------------------------+

.. note:: The MOK directory contains the private key used for signing kernel
	modules. It is highly suggested to use disk encryption to avoid leaking
	this key to a malicious attacker. Please be aware of the risk.

Ubuntu and Debian Based Systems
-------------------------------

Prior to installing the driver, enable secure boot on the target system. If
secure boot is enabled during driver installation, a MOK will be automatically
generated during install, if it doesn't already exist in the MOK directory,
and the user will be prompted to enter a temporary password to import the MOK
certificate into the system. This prompt uses a text mode tool, which can be
navigated using TAB to highlight, and ENTER to select.

Reboot the system and the MOK manager will start automatically.

See :ref:`Using-the-MOK-Manager` for further instructions.

RHEL/CentOS Systems
-------------------

To use secure boot with this driver stack, you must manually import the MOK
certificate onto your system. The driver installation will automatically
generate a MOK if it doesn't already exist in the MOK directory.

To import the MOK certificate, first install the "mokutil" package using your
package manager:

.. code-block:: bash

  sudo yum install mokutil

Then run mokutil to import the certificate:

.. code-block:: bash

  sudo mokutil --import /root/mok.der

Mokutil will require you to provide a temporary password for importing the
certificate. Reboot the system and the MOK manager will start automatically.

See :ref:`Using-the-MOK-Manager` for further instructions.

SLE Systems
-----------

Signing support for dkms requires dkms version 2.8, which is unavailable in
SLE. If DKMS is updated manually by the user, signing can be used in the same
way as RHEL, but this is untested and might not work as intended. Please use at
your own risk.

.. _Using-the-MOK-Manager:

Using the MOK Manager to Enroll the MOK Certificate
---------------------------------------------------

After requesting to import the MOK certificate and entering a temporary
password, the MOK manager will start on next boot.

Hit any key to avoid continuing boot. Do no select "Continue Boot", but rather
select "Enroll MOK" and "View key 0" to confirm that the key is correct.

Select "Continue", "Yes", and enter the temporary password to finish enrolling
the key certificate. Select "Reboot" to exit the MOK manager.

The signed driver should now load successfully.

.. _Packages-Containing-Executable-Binaries:

Packages Containing Executable Binaries
=======================================

A few packages do provide executables which are installed to either
`/opt/amdgpu/bin` or `/opt/amdgpu-pro/bin`, but are not added to any default
paths. For example:

- clinfo-amdgpu-pro
- drm-utils-amdgpu
- llvm-amdgpu

There are several options for running these programs without specifying the full
paths.

.. _Update-PATH-on-Current-Shell:

Update PATH on Current Shell
----------------------------

The PATH in the current operating shell can be updated to include the directories

.. code-block:: bash

  export PATH=/opt/amdgpu-pro/bin:/opt/amdgpu/bin:$PATH

.. _Update-PATH-for-User-Profile:

Update PATH for User Profile
----------------------------

Most shells have user files in the user's home directory that are automatically
called when a shell starts. Updating this file with:

.. code-block:: bash

  export PATH=/opt/amdgpu-pro/bin:/opt/amdgpu/bin:$PATH

Will ensure that everytime that shell starts, the path is already updated.

Common shell/profile files:

- bash - $HOME/.bashrc
- csh - $HOME/.cshrc
- ksh - $HOME/.kshrc
- zsh - $HOME/.zshrc

After adding the line to the applicable file, it can be checked by starting a
new shell and printing out the path.

.. code-block:: bash

  echo $PATH

.. _Uninstalling-the-AMDGPU-stack:

Uninstalling the AMDGPU stack
=============================

To remove all components of the stack, run the uninstall script from anywhere
in the system.

1. To uninstall all components, run the following command:

   .. code-block:: bash

    $ amdgpu-uninstall
    # or the following is also equivalent:
    $ amdgpu-install --uninstall

2. After uninstalling the components, reboot the target system.

3. For complete uninstall, remember to also remove the installer package
   "amdgpu-install" using your package manager:

.. code-block:: bash

 # For Ubuntu:
 $ sudo apt-get purge amdgpu-install
 # For RHEL:
 $ sudo yum remove amdgpu-install
 # For SLE:
 $ sudo zypper remove amdgpu-install
