#####################################
Using the ``amdgpu-install`` Script
#####################################

.. contents:: Table of Contents

----------

Script Types
============

The AMDGPU stack offers the following script to help you install a coherent set
of stack components:

- ``amdgpu-install``

This script supports a set of arguments that are explained in the following
sections of this page.

------------

Purpose
=======

The ``amdgpu-install`` script does the following:

- Simplifies the installation of the AMDGPU stack by encapsulating the
  distribution specific package installation logic and by using command line
  options that allow to specify the:

   * Use case of the AMDGPU stack to be installed (All-Open or Workstation)
   * Combination of components (in case of the Pro stack, or user selection)

- Performs post-install checks to verify whether the installation was performed
  successfully.
- Installs the uninstallation script to allow you to remove the whole
  AMDGPU stack from the system by using a single command.

The script is provided by the installer package.

---------

Invoking the ``amdgpu-install`` Script
=======================================

Once you have installed the installer package, invoke the script as
``amdgpu-install`` from a terminal.

.. note:: Use of the ``-y`` option is recommended to avoid prompts from the
      package manager.

-----------------

Scenarios
=========

Specifying an OpenCL Implementation
-----------------------------------

There are two OpenCL implementations available:

- **ROCr**: Provides support for *Vega 10* and newer hardware.
- **Legacy**: Provides support for hardware older than *Vega 10*.
    (**DEPRECATED**, requires EULA acceptance)

You can either install one or both implementations.

.. note:: To use the ROCr implementation of OpenCL, the running user might
      need additional permissions. Usually the user must be added to the
      "render" group or to the "video" group. See the notes in
      :ref:`Installation-OpenCL-Optional-Component` for more details.

OpenCL is installed using the ``--opencl`` option, as follows:

+------------------------+-----------------------------------------------------+
|**Option**              | **Description**                                     |
+------------------------+-----------------------------------------------------+
|``--opencl=rocr``       | Installs ROCr OpenCL.                               |
+------------------------+-----------------------------------------------------+
|``--opencl=legacy``     | Installs legacy OpenCL (**DEPRECATED Proprietary**) |
+------------------------+-----------------------------------------------------+
|``--opencl=rocr,legacy``| Installs both ROCr and legacy OpenCL.               |
+------------------------+-----------------------------------------------------+

Applicable examples on this page demonstrate installation of both OpenCL
implementations.


.. _Installation-Vulkan-Optional-Component:

Specifying a Vulkan Implementation
-----------------------------------

There are two Vulkan implementations available:

- **AMDVLK**: Provides open source Vulkan
- **PRO**: Provides PRO vulkan (**Proprietary**, requires EULA acceptance)

You can either install one or both implementations.

Vulkan is installed using the ``--vulkan`` option, as follows:

+------------------------+---------------------------------------------+
|**Option**              | **Description**                             |
+------------------------+---------------------------------------------+
|``--vulkan=amdvlk``     | Installs open source Vulkan                 |
+------------------------+---------------------------------------------+
|``--vulkan=pro``        | Installs PRO Vulkan (**Proprietary**)       |
+------------------------+---------------------------------------------+
|``--vulkan=amdvlk,pro`` | Installs both open and PRO Vulkan.          |
+------------------------+---------------------------------------------+

Viewing Help or Usage Output
-----------------------------
Executing the script with ``-h`` or ``--help`` argument provides the help or
usage output.

.. code-block:: bash

  $ amdgpu-install -h


Installing Non-Interactively
-----------------------------

Using the argument ``-y`` informs the package management system to assume
the answer *yes* for any interactive question. The ``--accept-eula`` option
will skip the first time install prompt for the user to accept the EULA.

These options allow the install script to embed itself inside other automated
scripts.

Example:

.. code-block:: bash

  $ amdgpu-install -y --accept-eula

.. note:: The ``--accept-eula`` option is only required for proprietary
      components, which require acceptance of the EULA. By using this option,
      you are confirming that you have read and agreed to be bound by the terms
      and conditions of the EULA (/usr/share/amdgpu-insall/AMDGPUPROEULA) for
      use of AMD Proprietary components.


Adding Advanced Package Management Functionality
---------------------------------------------------

More advanced functionality can be obtained by providing the installation
script with arguments to pass through to the package management software of the
distribution you are using.

- On Ubuntu, refer to the `apt man page <https://linux.die.net/man/8/apt>`_ for more information.
- On Red Hat, refer to `yum man page <https://linux.die.net/man/8/yum>`_ for more information.
