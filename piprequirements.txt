sphinx < 2
docutils < 0.18
sphinx_rtd_theme < 0.5
readthedocs-sphinx-ext < 2.2
Jinja2 < 3.1.0
